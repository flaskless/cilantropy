from cilantropy.cilantropy import blueprint
import flask

app = flask.Flask(__name__)
app.register_blueprint(blueprint=blueprint)

if __name__ == '__main__':
    app.run()
